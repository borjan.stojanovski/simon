-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2019 at 02:04 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `first_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `company` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `num_emp` varchar(64) NOT NULL,
  `department` varchar(64) NOT NULL,
  `textarea` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `company`, `email`, `phone`, `num_emp`, `department`, `textarea`) VALUES
(1, 'Borjan', 'Stojanovski', 'Schatze', 'borjan@schatze.com.mk', '38970344039', '0', 'Marketing', 'Zdravo'),
(2, 'Borjan', 'Stojanovski', 'Schatze', 'borjan@schatze.com.mk', '38970344039', '0', 'Marketing', 'Zdravo'),
(3, 'Borjan', 'Stojanovski', 'Schatze', 'borjan@schatze.com.mk', '38970344039', '0', 'Marketing', 'Zdravo'),
(4, 'dejan', 'Spasovski', 'Schatze', 'd.spasovski@schatze.com.mk', '3897102654', '1', 'Sales', 'sdadsadsa'),
(5, 'admin', 'Admin', 'admin', 'admin@gmail.com', '356654789', '2', 'Product', 'dsadsadsa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
