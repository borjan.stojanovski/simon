<?php 
include '/xampp/htdocs/project1/Arrays/array.php';

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Brainster Tool Box</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" type="text/css" media="screen" href="style/firstproject.css"/>
    
</head>

<body>

    <!-- navigation bar -->
<?php include 'Components\menu.php'?>

    <!-- main div -->
<?php include 'components\about.php' ?>

    <!-- all games -->
    <section class="games games-margin">
            <div class="container">
                <div class="row">
                <?php
            if(count($games) == 0) {
                echo "<div class='box'>Still no games, come later</div>";
            } else {
                for($i=0; $i<count($games);$i++) {
                    echo "
                            <div class = 'col-md-4 col-sm-6'>
                                <div class ='panel panel-default'>
                                <a href='gamepage.php?gameIndex=$i' style='color:unset;' >
                                <div class='panel-heading'><img src=" . $games[$i]['img'] . " width = '100%' height= '250px'; class='img-radius view overlay zoom'></div>
                                    <div class='panel-body'>
                                        <div class='row'>
                                            <div class='col-md-9 col-sm-9 col-xs-9'>
                                                <p class='font-size-tittle bold'>" . $games[$i]['game-tittle'] . "</p>
                                                <p class='h6'>Категорија: <span class='aqua-blue-color bold'>" . $games[$i]['game-category'] . "</span></p>
                                            </div>
                                            <div class='col-md-3 col-sm-3 col-xs-3'>
                                                 <div class='game-logo-edit'>
                                                 <img src=" . $games[$i]['game-logo'] . "  class='pull-right' height='65px' style='padding:15px;'>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class = 'row fa-padding'>
                                            <div class = 'col-md-1 col-xs-1'>
                                                <i class='far fa-clock' style='font-size:24px'></i>
                                            </div>
                                            <div class = 'col-md-11 col-xs-11'>
                                            <p class='bold'>Времетраење:" . "<br><span class='gray'>" . $games[$i]['game-time'] . "</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>";
                }
            }
                ?>
                </div>
            </div>
    </section>

    <!-- lastdiv-text -->
<?php include 'components\foot.php' ?>
 
    <!-- footer -->
 <?php include 'components\footer.php' ?>


</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }

    $(function(){
        $(document.body).on('show.bs.modal', function () {
            $(window.document).find('html').addClass('modal-open');
        });
        $(document.body).on('hide.bs.modal', function () {
            $(window.document).find('html').removeClass('modal-open');
        });
    });
</script>
</html> 