<?php
include "Arrays/games.php";
if(isset($_GET['gameIndex']) && isset($game[$_GET['gameIndex']])) {
    $gameIndex = $_GET['gameIndex'];
    $game = $game[$gameIndex];
} else {
    header("Location: firstproject.php");
    die();
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= $game["Header"] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
    <link rel="stylesheet" type="text/css" media="screen" href="style/gamepage.css"/>
</head>

<body>

    <!-- navigation bar -->
<?php include 'components\menu.php' ?>
    <!-- background -->
    <section class="background" style="background-image: url(<?= $game["Image"] ?>)">
    </section>

    <!-- game-explain -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="game-header"><?= $game["Header"] ?></h1>
            </div>
        </div>
        <div class="row  gray-bg text-center ">
            <div class="col-md-3">
                <i class='far fa-clock fafont' style='font-size:20px'></i><span class="gray-bg">Времетраење:</span>
                <p class="gray col-md-8 text-center"> <?= $game["Time"]?></p>
            </div>
            <div class="col-md-3">
                <i class='fas fa-user-friends fafont' style='font-size:20px'></i><span class="gray-bg">Големина на група:</span>
                <p class="gray col-md-4"> <?= $game["Group-size"]?></p>
            </div>
            <div class="col-md-3">
                <i class='fa fa-university fafont' style='font-size:20px'></i><span class="gray-bg">Ниво:</span>
                <p class="gray col-md-10"> <?= $game["Facilitation"]?></p>
            </div>
            <div class="col-md-3">
                <i class='fas fa-paint-roller fafont' style='font-size:20px'></i><span class="gray-bg">Материјали:</span>
                <p class="gray"> <?= $game["Materials"]?></p>
            </div>
        </div>
        <div class="row explanation-margin">
            <div class="col-md-12">
                <?= $game['Explanation'] ?>
            </div>
            <div class="col-md-12"> 
                <?= $game['Steps'] ?>
            </div>
        </div>
    </div>

    <!-- lastdiv-text -->
    <?php include 'components\foot.php' ?>
 
 <!-- footer -->
<?php include 'components\footer.php' ?>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
    function w3_open() {
        document.getElementById("mySidebar").style.display = "block";
        document.getElementById("myOverlay").style.display = "block";
    }

    function w3_close() {
        document.getElementById("mySidebar").style.display = "none";
        document.getElementById("myOverlay").style.display = "none";
    }

    $(function () {
      $(document).scroll(function () {
        var $nav = $(".navbar-fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
      });
    });
</script>
</html> 