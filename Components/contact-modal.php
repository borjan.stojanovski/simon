<?php include 'db\dbconect.php'?>


<div id="contactModal" class="modal fade" role="dialog">
    <div class="modal-dialog" >
        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
            <!-- save.php is yet to be done -->
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Регистрирај се</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="name">Firstname:</label>
                        <input type="text" name="name" class="form-control" id="name">
                                            </div>
                    <div class="form-group">
                        <label for="lastname">Lastname:</label>
                        <input type="text" name="lastname" class="form-control" id="lastname">
                    </div>
                    <div class="form-group">
                        <label for="companyname">Company:</label>
                        <input type="text" name="companyname" class="form-control" id="companyname">
                    </div>
                    <div class="form-group">
                        <label for="email">Email address:</label>
                        <input type="email" name="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone number:</label>
                        <input type="number" name="phone" class="form-control" id="phone">
                    </div>
                    <div class="form-group">
                        <label for="numOfEmployees">Number of employees</label>
                        <select name="numOfEmployes" id="numOfEmployees" class="form-control">
                            <option value="0">1</option>
                            <option value="1">2-10</option>
                            <option value="2">11-50</option>
                            <option value="3">51-200</option>
                            <option value="4">200+</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="department">Department</label>
                        <select name="department" id="department" class="form-control">
                            <option value="-1">Одбери Оддел</option>
                            <option value="HR">Човечки Ресурски</option>
                            <option value="Marketing">Маркетинг</option>
                            <option value="Product">Продукт</option>
                            <option value="Sales">Продажба</option>
                            <option value="CEO">CEO</option>
                            <option value="Other">Друго</option>
                        </select>
                    </div>
                    <div class="amber-textarea active-amber-textarea">
                        <i class="fas fa-pencil-alt prefix"></i>
                        <textarea name="text" type="text" id="form18" class="md-textarea form-control" rows="3" placeholder="Please use this space to tell us more about your needs."></textarea>
                        <label for="form18"></label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>