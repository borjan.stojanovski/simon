    <section class="main-div jumptarget clippy-div" id="goto">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12 text-center custom-padding-text">
                    <p class="white">Изработено од студенти за програмирање на <a href="https://brainster.co/"><span class="my-yellow sidenav-padding bold"><ins>Brainster</ins></span></a></p>
                </div>

                <div class="col-md-12">
                    <p class="header-edit bold white text-center">FUTURE-PROOF YOUR ORGANIZATION</p>
                    <div class="col-md-offset-2 col-md-8">
                        <p class="text-center white custom-padding-text text-edit">Find out how to unlock progress in
                            your
                            business. Understand your current state, indentify opportunity arreas and prepare to
                            take chareg of your organizations future.</p>
                    </div>
                    <div class="col-md-12 text-center">
                        <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"><button class="btn third-button black-color bold">Take the assessment</button></a></a>
                    </div>
                </div>
            </div>
        </div>
    </section>