<?php include 'components\contact-modal.php'?>
    
    <!-- navigation bar -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div class="w3-sidebar w3-bar-block w3-animate-left" style="display:none;z-index:5" id="mySidebar">
                    <img class="pull-left logo-edit" src="images/logo.png">
                    <button class="w3-bar-item w3-button w3-large pull-right close-button" onclick="w3_close()">&times;
                        Затвори</button>
                    <a class="sidenav-padding my-yellow border-bottom bold w3-bar-item w3-button" href="#">Регистрирај се</a>
                    <a class="my-yellow border-bottom bold w3-bar-item w3-button" href="#">Најави се</a>
                    <a class="black-color border-bottom bold w3-bar-item w3-button" href="">За нас</a>
                    <a class="black-color border-bottom bold w3-bar-item w3-button" href="https://www.facebook.com/pg/brainster.co/photos/" target="_blank">Галерија</a>
                    <a  class="black-color border-bottom bold w3-bar-item w3-button" id="showFormModal" data-toggle="modal" data-target="#contactModal" href="#">Контакт</a>
                </div>
                <div class="w3-overlay w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" id="myOverlay"></div>
                <div>
                    <i class="fa fa-bars my-yellow" style="font-size:20px;cursor:pointer; color:#f9d42;" onclick="w3_open()">
                        Мени</i>
                    <a href="#goto"><img class="img-background" src="images/logo.png"></a>
                </div>
            </div>
            <button type="button" class="btn first-button custom-btn-edit my-yellow bold pull-right orginal-button" id="showFormModal" data-toggle="modal" data-target="#contactModal">Вработи
                    наши студенти</button>

            <a href="https://www.brainster.io/business" target="_blank"><button class="btn second-button black-color custom-btn-edit bold pull-right orginal-button">Обуки
                    за компании</button></a>

        </div>
    </nav>
