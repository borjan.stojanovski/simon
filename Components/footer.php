   <footer>
        <div class="container-fluid text-center">
            <div class="row row-margin">
                <div class="col-md-4 col-sm-4 col-margin bold">
                    <span class="white word-margin">About us</span>
                    <span class="white word-margin">Contact</span>
                    <span class="white">Gallery</span>
                </div>
                <div class="col-md-4 col-sm-4 img-margin">
                    <img class="img-footer" src="images/logo.png">
                </div>
                <div class="col-md-4 col-sm-4 col-margin">
                    <a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i class="fab fa-linkedin-in fafonts word-margin"></i></a>
                    <a href="https://twitter.com/BrainsterCo" target="_blank"><i class="fab fa-twitter fafonts word-margin"></i></a>
                    <a href="https://www.facebook.com/brainster.co" target="_blank"> <i class="fab fa-facebook-f fafonts"></i></a>
                </div>
            </div>
        <div class="row mobile-footer">
                <div class="col-md-12 mobile-footer-margin">
                    <a href="https://www.linkedin.com/school/brainster-co/" target="_blank"><i class="fab fa-linkedin-in fafonts word-margin"></i></a>
                    <a href="https://twitter.com/BrainsterCo" target="_blank"><i class="fab fa-twitter fafonts word-margin"></i></a>
                    <a href="https://www.facebook.com/brainster.co" target="_blank"> <i class="fab fa-facebook-f fafonts"></i></a>
                </div>
                <div class="col-md-12 mobile-footer-margin">
                    <img class="img-mobile" src="images/logo.png">
                </div>
                <div class="col-md-12">
                    <span class="white word-margin">About us</span>
                    <span class="white word-margin">Contact</span>
                    <span class="white">Gallery</span>
                </div>
        </div>
            </div>
    </footer>