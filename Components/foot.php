    <section class="bg-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="header-edit bold white text-center custom-header-padding">FUTURE-PROOF YOUR ORGANIZATION</p>
                    <div class="col-md-offset-2 col-md-8">
                        <p class="text-center white custom-padding-text text-edit">Find out how to unlock progress in
                            your
                            business. Understand your current state, indentify opportunity arreas and prepare to
                            take chareg of your organizations future.</p>
                    </div>
                    <div class="col-md-12 text-center">
                        <a href="https://brainsterquiz.typeform.com/to/kC2I9E" target="_blank"><button class="btn fourth-button black-color bold">Take the assessment</button></a></a>
                    </div>
                </div>
            </div>
            </div>
    </section>
